using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day5 : Day{
        public override int Expected1 { get; set; } = 820;
        public override int Expected2 { get; set; } = 821;

        public override void Run(){
            var takenSeats = new bool[979];
            foreach(var line in Input){
                var row = 0;
                var col = 0;
                for(int i = 0; i < line.Length; i++){
                    if(line[i] == 'B'){
                        row = row | (int)Math.Pow(2, 6 - i);
                    }
                    else if(line[i] == 'R'){
                        col = col | (int)Math.Pow(2, 9 - i);
                    }
                }
                var id = row * 8 + col;
                takenSeats[id] = true;
                Result1 = Math.Max(id, Result1);
            }
            var previousTaken = false;
            for (var i = 0; i < takenSeats.Length; i++){
                if(previousTaken && !takenSeats[i]){
                    Result2 = i;
                }
                previousTaken = takenSeats[i];
            }
        }
    }
}