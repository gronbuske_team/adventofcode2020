using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day19 : Day{
        public override long Expected1 { get; set; } = 3;
        public override long Expected2 { get; set; } = 12;

        private class Rule{
            public int Num {get;set;}
            public bool A {get;set;} = false;
            public bool B {get;set;} = false;

            public List<List<Rule>> refs{get;set;}

            public List<string> TestRules(List<string> input){
                if(input == null) return null;
                var results = new List<string>();
                foreach(var s in input){
                    if (A){
                        if(s.StartsWith('a')){
                            results.Add(s.Substring(1));
                        }
                        continue;
                    }
                    if (B){
                        if(s.StartsWith('b')){
                            results.Add(s.Substring(1));
                        }
                        continue;
                    }
                    foreach(var re in refs){
                        var testInput = new List<string>{s};
                        foreach(var rule in re){
                            testInput = rule.TestRules(testInput);
                        }
                        if(testInput == null) continue;
                        foreach(var i in testInput.Distinct()) results.Add(i);
                    }
                }
                if(!results.Any()) return null;
                if(Num != 0) results = results.Distinct().ToList();
                return results;
            }
        }

        public override void Run(){
            var rules = new Rule[Math.Max(Input.Count(x => x.Any() && char.IsDigit(x[0])), 43)];
            foreach(var line in Input.Where(x => x.Any() && char.IsDigit(x[0]))){
                var steps = line.Split(':')[1].Trim().Split(' ');
                var rule = new Rule();
                rule.Num = int.Parse(line.Split(':')[0]);
                if(steps[0] == "\"a\"") rule.A = true;
                else if(steps[0] == "\"b\"") rule.B = true;
                rules[int.Parse(line.Split(':')[0])] = rule;
            }
            foreach(var line in Input.Where(x => x.Any() && char.IsDigit(x[0]))){
                var steps = line.Split(':')[1].Trim().Split(' ');
                if(steps[0] != "\"a\"" && steps[0] != "\"b\"") {
                    var refs = new List<List<Rule>>();
                    var list = new List<Rule>();
                    foreach(var step in steps){
                        if(step == "|"){
                            refs.Add(list);
                            list = new List<Rule>();
                            continue;
                        }
                        list.Add(rules[int.Parse(step)]);
                    }
                    refs.Add(list);
                    rules[int.Parse(line.Split(':')[0])].refs = refs;
                }
            }
            var res = rules[0].TestRules(Input.Where(x => x.Any() && (x[0] == 'a' || x[0] == 'b')).ToList());
            Result1 = res.Count(x => x.Count() == 0);
            
            rules = new Rule[Math.Max(Input.Count(x => x.Any() && char.IsDigit(x[0])), 43)];
            foreach(var line in Input.Where(x => x.Any() && char.IsDigit(x[0]))){
                var steps = line.Split(':')[1].Trim().Split(' ');
                var rule = new Rule();
                rule.Num = int.Parse(line.Split(':')[0]);
                if(steps[0] == "\"a\"") rule.A = true;
                else if(steps[0] == "\"b\"") rule.B = true;
                rules[int.Parse(line.Split(':')[0])] = rule;
            }
            foreach(var line in Input.Where(x => x.Any() && char.IsDigit(x[0]))){
                var steps = line.Split(':')[1].Trim().Split(' ');
                if(steps[0] != "\"a\"" && steps[0] != "\"b\"") {
                    var refs = new List<List<Rule>>();
                    var list = new List<Rule>();
                    foreach(var step in steps){
                        if(step == "|"){
                            refs.Add(list);
                            list = new List<Rule>();
                            continue;
                        }
                        list.Add(rules[int.Parse(step)]);
                    }
                    refs.Add(list);
                    rules[int.Parse(line.Split(':')[0])].refs = refs;
                }
            }
            var rule8 = new List<Rule>();
            rule8.Add(rules[42]);
            rule8.Add(rules[8]);
            rules[8].refs.Add(rule8);
            var rule11 = new List<Rule>();
            rule11.Add(rules[42]);
            rule11.Add(rules[11]);
            rule11.Add(rules[31]);
            rules[11].refs.Add(rule11);
            res = rules[0].TestRules(Input.Where(x => x.Any() && (x[0] == 'a' || x[0] == 'b')).ToList());
            Result2 = res.Count(x => x.Count() == 0);
        }
    }
}