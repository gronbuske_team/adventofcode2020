using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day8 : Day{
        public override long Expected1 { get; set; } = 5;
        public override long Expected2 { get; set; } = 8;

        private bool TryCorrect(List<string> input, int opNumber){
            var index = 0;
            Result2 = 0;
            var counter = 0;
            while(index >= 0 && index < input.Count()){
                if(input[index] == null)
                    return false;
                var split = input[index].Trim().Split(" ");
                input[index] = null;
                var nr = int.Parse(split[1].Substring(1));
                nr = split[1][0] == '-' ? -nr : nr;
                switch(split[0]){
                    case "jmp":
                        if(counter == opNumber){
                            index++;
                            counter++;
                            break;
                        }
                        counter++;
                        index += nr;
                        break;
                    case "acc":
                        Result2 += nr;
                        index++;
                        break;
                    case "nop":
                        if(counter == opNumber){
                            index += nr;
                            counter++;
                            break;
                        }
                        counter++;
                        index++;
                        break;
                    default:
                        throw new Exception();
                }
            }
            return index == input.Count();
        }

        public override void Run(){
            var index = 0;
            var firstRun = new List<string>(Input);
            while(firstRun[index] != null){
                var split = firstRun[index].Trim().Split(" ");
                firstRun[index] = null;
                var nr = int.Parse(split[1].Substring(1));
                nr = split[1][0] == '-' ? -nr : nr;
                switch(split[0]){
                    case "jmp":
                        index += nr;
                        break;
                    case "acc":
                        Result1 += nr;
                        index++;
                        break;
                    case "nop":
                        index++;
                        break;
                    default:
                        throw new Exception();
                }
            }
            var counter = 0;
            var input = new List<string>(Input);
            while(!TryCorrect(input, counter)){
                counter++;
                input = new List<string>(Input);
            }
        }
    }
}