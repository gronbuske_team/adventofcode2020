using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day18 : Day{
        public override long Expected1 { get; set; } = 26457;
        public override long Expected2 { get; set; } = 694173;

        private void Part1(){
            foreach(var line in Input){
                var operators = new List<string>();
                foreach(var c in line){
                    if (c == '(' || c == '+' || c == '*') operators.Add(c.ToString());
                    else if (c == ')'){
                        var num = operators[^1];
                        operators.RemoveRange(operators.Count - 2, 2);
                        if(operators.Count == 0 || operators[^1] == "("){
                            operators.Add(num);
                            continue;
                        }
                        var op = operators[^1];
                        var ln = operators[^2];
                        operators.RemoveRange(operators.Count - 2, 2);
                        if (op == "+"){
                            operators.Add((long.Parse(ln) + long.Parse(num)).ToString());
                        } else if (op == "*"){
                            operators.Add((long.Parse(ln) * long.Parse(num)).ToString());
                        }
                    }
                    else if(long.TryParse(c.ToString(), out var number)){
                        if(operators.Count == 0 || operators[^1] == "("){
                            operators.Add(number.ToString());
                            continue;
                        }
                        var op = operators[^1];
                        var ln = operators[^2];
                        operators.RemoveRange(operators.Count - 2, 2);
                        if (op == "+"){
                            operators.Add((long.Parse(ln) + number).ToString());
                        } else if (op == "*"){
                            operators.Add((long.Parse(ln) * number).ToString());
                        }
                    }
                }
                Result1 += long.Parse(operators[0]);
            }
        }
        private void Part2(){
            foreach(var line in Input){
                var operators = new List<string>();
                foreach(var c in line){
                    if (c == '(' || c == '+' || c == '*') operators.Add(c.ToString());
                    else if (c == ')'){
                        var lastPar = operators.FindLastIndex(x => x == "(");
                        for(var i = lastPar + 3; i < operators.Count; i+=2){
                            if(operators[i - 1] == "+") {
                                operators[i-2] = (long.Parse(operators[i-2]) + long.Parse(operators[i])).ToString();
                                operators.RemoveRange(i - 1, 2);
                                i -= 2;
                            }
                        }
                        var result = long.Parse(operators[lastPar + 1]);
                        for(var i = lastPar + 3; i < operators.Count; i+=2){
                            result *= long.Parse(operators[i]);
                        }
                        operators.RemoveRange(lastPar, operators.Count - lastPar);
                        operators.Add(result.ToString());
                    }
                    else if(long.TryParse(c.ToString(), out var number)){
                        if(operators.Count == 0 || operators[^1] != "+"){
                            operators.Add(number.ToString());
                            continue;
                        }
                        var ln = operators[^2];
                        operators.RemoveRange(operators.Count - 2, 2);
                        operators.Add((long.Parse(ln) + number).ToString());
                    }
                }
                for(var i = 2; i < operators.Count; i+=2){
                    if(operators[i - 1] == "+") {
                        operators[i-2] = (long.Parse(operators[i-2]) + long.Parse(operators[i])).ToString();
                        operators.RemoveRange(i - 1, 2);
                        i -= 2;
                    }
                }
                var result2 = long.Parse(operators[0]);
                for(var i = 2; i < operators.Count; i+=2){
                    if(operators[i - 1] == "*") result2 *= long.Parse(operators[i]);
                }
                Result2 += result2;
            }
        }

        public override void Run(){
            Part1();
            Part2();
        }
    }
}