using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day6 : Day{
        public override int Expected1 { get; set; } = 11;
        public override int Expected2 { get; set; } = 6;

        public override void Run(){
            var answers = new Dictionary<char, int>();
            var peopleInGroup = 0;
            foreach(var line in Input){
                if(string.IsNullOrWhiteSpace(line)){
                    Result1 += answers.Count;
                    Result2 += answers.Where(x => x.Value == peopleInGroup).Count();
                    answers = new Dictionary<char, int>();
                    peopleInGroup = 0;
                    continue;
                }
                peopleInGroup++;
                foreach(var c in line){
                    if(!answers.TryAdd(c, 1)){
                        answers[c]++;
                    }
                }
            }
            Result1 += answers.Count;
            Result2 += answers.Where(x => x.Value == peopleInGroup).Count();
        }
    }
}