using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day10 : Day{
        public override long Expected1 { get; set; } = 220;
        public override long Expected2 { get; set; } = 19208;

        public override void Run(){
            var jolts = Input.Select(x => long.Parse(x)).Append(0).OrderBy(x => x).ToList();
            var paths = new long[jolts.Count];
            var currentJolt = 0L;
            var ones = 0;
            var threes = 0;
            Result2 = 1;
            for(var i = 0; i < jolts.Count; i++){
                if(jolts[i] - currentJolt == 1){
                    ones++;
                    Result2 += i >= 2 && jolts[i] - jolts[i-2] <= 3 ? paths[i-2] : 0;
                    Result2 += i >= 3 && jolts[i] - jolts[i-3] <= 3 ? paths[i-3] : 0;
                }
                else if(jolts[i] - currentJolt == 2){
                    Result2 += i >= 2 && jolts[i] - jolts[i-2] == 3 ? paths[i-2] : 0;
                }
                else if(jolts[i] - currentJolt == 3){
                    threes++;
                }
                currentJolt = jolts[i];
                paths[i] = Result2;
            }
            threes++;
            Result1 = ones * threes;
        }
    }
}