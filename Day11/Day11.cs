using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day11 : Day{
        public override long Expected1 { get; set; } = 37;
        public override long Expected2 { get; set; } = 26;

        public enum SeatStatus{
            Floor,
            TakenWasEmpty,
            EmptyWasTaken,
            TakenWasTaken,
            EmptyWasEmpty
        }

        private void Part1(){
            var changed = true;
            var seats = Input.Select(x => x.Select(y => {return y == '.' ? SeatStatus.Floor : y == '#' ? SeatStatus.TakenWasTaken : SeatStatus.EmptyWasEmpty;}).ToList()).ToList();
            while (changed){
                changed = false;
                Result1 = 0;
                for (int r = 0; r < seats.Count; r++){
                    for (int s = 0; s < seats[r].Count; s++){
                        var status = seats[r][s];
                        if (status == SeatStatus.Floor){
                            continue;
                        }
                        var neighbors = 0;
                        if (r > 0){
                            if (s > 0){
                                neighbors += seats[r-1][s-1] == SeatStatus.EmptyWasTaken || seats[r-1][s-1] == SeatStatus.TakenWasTaken ? 1 : 0;
                            }
                            neighbors += seats[r-1][s] == SeatStatus.EmptyWasTaken || seats[r-1][s] == SeatStatus.TakenWasTaken ? 1 : 0;
                            if (s < seats[r].Count - 1){
                                neighbors += seats[r-1][s+1] == SeatStatus.EmptyWasTaken || seats[r-1][s+1] == SeatStatus.TakenWasTaken ? 1 : 0;
                            }
                        }
                        if (s > 0){
                            neighbors += seats[r][s-1] == SeatStatus.EmptyWasTaken || seats[r][s-1] == SeatStatus.TakenWasTaken ? 1 : 0;
                        }

                        if (r < seats.Count - 1){
                            if (s > 0){
                                neighbors += seats[r+1][s-1] == SeatStatus.TakenWasTaken || seats[r+1][s-1] == SeatStatus.TakenWasEmpty ? 1 : 0;
                            }
                            neighbors += seats[r+1][s] == SeatStatus.TakenWasTaken || seats[r+1][s] == SeatStatus.TakenWasEmpty ? 1 : 0;
                            if (s < seats[r].Count - 1){
                                neighbors += seats[r+1][s+1] == SeatStatus.TakenWasTaken || seats[r+1][s+1] == SeatStatus.TakenWasEmpty ? 1 : 0;
                            }
                        }
                        if (s < seats[r].Count - 1){
                            neighbors += seats[r][s+1] == SeatStatus.TakenWasTaken || seats[r][s+1] == SeatStatus.TakenWasEmpty ? 1 : 0;
                        }
                        if (neighbors == 0 && (status == SeatStatus.EmptyWasTaken || status == SeatStatus.EmptyWasEmpty)){
                            seats[r][s] = SeatStatus.TakenWasEmpty;
                            changed = true;
                        }
                        else if (neighbors > 3 && (status == SeatStatus.TakenWasEmpty || status == SeatStatus.TakenWasTaken)){
                            seats[r][s] = SeatStatus.EmptyWasTaken;
                            changed = true;
                        }
                        else if (status == SeatStatus.TakenWasEmpty) seats[r][s] = SeatStatus.TakenWasTaken;
                        else if (status == SeatStatus.EmptyWasTaken) seats[r][s] = SeatStatus.EmptyWasEmpty;
                        if (status == SeatStatus.TakenWasTaken || status == SeatStatus.TakenWasEmpty) Result1++;
                    }
                }
            }
        }
        private void Part2(){
            var changed = true;
            var seats = Input.Select(x => x.Select(y => {return y == '.' ? SeatStatus.Floor : y == '#' ? SeatStatus.TakenWasTaken : SeatStatus.EmptyWasEmpty;}).ToList()).ToList();
            while (changed){
                changed = false;
                Result2 = 0;
                for (int r = 0; r < seats.Count; r++){
                    for (int s = 0; s < seats[r].Count; s++){
                        var status = seats[r][s];
                        if (status == SeatStatus.Floor){
                            continue;
                        }
                        var neighbors = 0;
                        var d = 0;
                        var h1 = false; var h2 = false; var h3 = false; var h4 = false;
                        while (!h1 || !h2 || !h3 || !h4){
                            d++;
                            if (r >= d){
                                if (!h1 && s >= d){
                                    h1 = seats[r-d][s-d] != SeatStatus.Floor;
                                    neighbors += seats[r-d][s-d] == SeatStatus.EmptyWasTaken || seats[r-d][s-d] == SeatStatus.TakenWasTaken ? 1 : 0;
                                }
                                if (!h2){
                                    h2 = seats[r-d][s] != SeatStatus.Floor;
                                    neighbors += seats[r-d][s] == SeatStatus.EmptyWasTaken || seats[r-d][s] == SeatStatus.TakenWasTaken ? 1 : 0;
                                }
                                if (!h3 && s + d < seats[r].Count){
                                    h3 = seats[r-d][s+d] != SeatStatus.Floor;
                                    neighbors += seats[r-d][s+d] == SeatStatus.EmptyWasTaken || seats[r-d][s+d] == SeatStatus.TakenWasTaken ? 1 : 0;
                                }
                            }
                            else {h1 = true; h2 = true; h3 = true;}
                            if (s >= d){
                                if (!h4){
                                    h4 = seats[r][s-d] != SeatStatus.Floor;
                                    neighbors += seats[r][s-d] == SeatStatus.EmptyWasTaken || seats[r][s-d] == SeatStatus.TakenWasTaken ? 1 : 0;
                                }
                            }
                            else {h4 = true;}
                        }

                        d = 0;
                        h1 = false; h2 = false; h3 = false; h4 = false;
                        while (!h1 || !h2 || !h3 || !h4){
                            d++;
                            if (r + d < seats.Count){
                                if (!h1 && s >= d){
                                    h1 = seats[r+d][s-d] != SeatStatus.Floor;
                                    neighbors += seats[r+d][s-d] == SeatStatus.TakenWasTaken || seats[r+d][s-d] == SeatStatus.TakenWasEmpty ? 1 : 0;
                                }
                                if (!h2){
                                    h2 = seats[r+d][s] != SeatStatus.Floor;
                                    neighbors += seats[r+d][s] == SeatStatus.TakenWasTaken || seats[r+d][s] == SeatStatus.TakenWasEmpty ? 1 : 0;
                                }
                                if (!h3 && s + d < seats[r].Count){
                                    h3 = seats[r+d][s+d] != SeatStatus.Floor;
                                    neighbors += seats[r+d][s+d] == SeatStatus.TakenWasTaken || seats[r+d][s+d] == SeatStatus.TakenWasEmpty ? 1 : 0;
                                }
                            }
                            else {h1 = true; h2 = true; h3 = true;}
                            if (s + d < seats[r].Count){
                                if(!h4){
                                    h4 = seats[r][s+d] != SeatStatus.Floor;
                                    neighbors += seats[r][s+d] == SeatStatus.TakenWasTaken || seats[r][s+d] == SeatStatus.TakenWasEmpty ? 1 : 0;
                                }
                            }
                            else {h4 = true;}
                        }

                        if (neighbors == 0 && (status == SeatStatus.EmptyWasTaken || status == SeatStatus.EmptyWasEmpty)){
                            seats[r][s] = SeatStatus.TakenWasEmpty;
                            changed = true;
                        }
                        else if (neighbors > 4 && (status == SeatStatus.TakenWasEmpty || status == SeatStatus.TakenWasTaken)){
                            seats[r][s] = SeatStatus.EmptyWasTaken;
                            changed = true;
                        }
                        else if (status == SeatStatus.TakenWasEmpty) {seats[r][s] = SeatStatus.TakenWasTaken; changed = true;}
                        else if (status == SeatStatus.EmptyWasTaken) {seats[r][s] = SeatStatus.EmptyWasEmpty; changed = true;}
                        if (status == SeatStatus.TakenWasTaken || status == SeatStatus.TakenWasEmpty) Result2++;
                    }
                }
            }
        }

        public override void Run(){
            Part1();
            Part2();
        }
    }
}