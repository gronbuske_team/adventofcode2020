using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day13 : Day{
        public override long Expected1 { get; set; } = 295;
        public override long Expected2 { get; set; } = 1068781;

        public override void Run(){
            // Console.WriteLine(LCM(4, 5, 2));
            var arrival = int.Parse(Input[0]);
            var buses = Input[1].Split(',');
            var leastWait = int.MaxValue;
            var leastId = 0;
            foreach(var bus in buses){
                if (bus == "x") continue;
                var busnum = int.Parse(bus);
                var wait = ((arrival / busnum) + 1) * busnum % arrival;
                if(wait < leastWait){
                    leastId = busnum;
                    leastWait = wait;
                }
            }
            Result1 = leastWait * leastId;

            var busList = new List<(int, int)>();
            for (var i = 0; i < buses.Count(); i++){
                if (buses[i] == "x") continue;
                busList.Add((i, int.Parse(buses[i])));
            }
            
            long x = 0;
            long step = 1;
            foreach(var bus in busList){
                while ((x + bus.Item1) % bus.Item2 != 0){
                    x += step;
                }
                step *= bus.Item2;
            }
            Result2 = x;
        }
    }
}