using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day14 : Day{
        public override long Expected1 { get; set; } = 51;
        public override long Expected2 { get; set; } = 208;

        public override void Run(){
            IEnumerable<char> mask = new List<char>();
            var mem = new Dictionary<long, long>();
            foreach(var line in Input){
                if(line.StartsWith("mask")){
                    mask = line.Split(" = ")[1].Reverse();
                }
                else{
                    var num = long.Parse(line.Split(" = ")[1]);
                    var addr = long.Parse(line.Split('[')[1].Split(']')[0]);
                    long val = 1;
                    long res = 0;
                    foreach(var m in mask){
                        var bit = num % 2;
                        num = num / 2;
                        if(m == 'X'){
                            res += val * bit;
                        } else if (m == '1'){
                            res += val;
                        }
                        val *= 2;
                    }
                    if(mem.ContainsKey(addr)) mem[addr] = res;
                    else mem.Add(addr, res);
                }
            }
            foreach(var x in mem){
                Result1 += x.Value;
            }
            
            mask = new List<char>();
            mem = new Dictionary<long, long>();
            foreach(var line in Input){
                if(line.StartsWith("mask")){
                    mask = line.Split(" = ")[1].Reverse();
                }
                else{
                    var num = long.Parse(line.Split(" = ")[1]);
                    var addr = long.Parse(line.Split('[')[1].Split(']')[0]);
                    long val = 1;
                    var addresses = new List<long>();
                    addresses.Add(0);
                    var xxx = addresses.Select(x => {
                                return x + 1;
                            });
                    foreach(var m in mask){
                        var bit = addr % 2;
                        addr = addr / 2;
                        if(m == 'X'){
                            var newAddr = addresses.Select(x => {
                                return x + val;
                            });
                            addresses = addresses.Concat(newAddr).ToList();
                        } else if (m == '1'){
                            addresses = addresses.Select(x => x + val).ToList();
                        }else if (bit == 1){
                            addresses = addresses.Select(x => x + val).ToList();
                        }
                        val *= 2;
                    }
                    foreach(var a in addresses){
                        if(mem.ContainsKey(a)) mem[a] = num;
                        else mem.Add(a, num);
                    }
                }
            }
            foreach(var x in mem){
                Result2 += x.Value;
            }
        }
    }
}