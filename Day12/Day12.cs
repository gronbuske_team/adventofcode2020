using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day12 : Day{
        public override long Expected1 { get; set; } = 25;
        public override long Expected2 { get; set; } = 286;

        private enum Direction{
            East = 0,
            South = 90,
            West = 180,
            North = 270
        }

        public override void Run(){
            var x1 = 0;
            var y1 = 0;
            var direction = Direction.East;
            var x2 = 0;
            var y2 = 0;
            var wpx = 10;
            var wpy = 1;
            foreach (var line in Input){
                var num = int.Parse(line.Substring(1));
                var order = line[0];
                switch (order){
                    case 'F':
                        x2 += wpx * num;
                        y2 += wpy * num;
                        switch (direction){
                            case Direction.East:
                                x1 += num;
                                break;
                            case Direction.West:
                                x1-= num;
                                break;
                            case Direction.North:
                                y1 += num;
                                break;
                            case Direction.South:
                                y1-= num;
                                break;
                        }
                        break;
                    case 'E':
                        wpx += num;
                        x1 += num;
                        break;
                    case 'W':
                        wpx -= num;
                        x1-= num;
                        break;
                    case 'N':
                        wpy += num;
                        y1 += num;
                        break;
                    case 'S':
                        wpy -= num;
                        y1-= num;
                        break;
                    case 'R':
                        direction = (Direction)(((int)direction + num) % 360);
                        if(num == 180){
                            wpx = -wpx;
                            wpy = -wpy;
                        } else if (num == 90){
                            var tmp = wpx;
                            wpx = wpy;
                            wpy = -tmp;
                        } else if (num == 270){
                            var tmp = wpx;
                            wpx = -wpy;
                            wpy = tmp;
                        }
                        break;
                    case 'L':
                        direction = (Direction)(((int)direction + 360 - num) % 360);
                        if(num == 180){
                            wpx = -wpx;
                            wpy = -wpy;
                        } else if (num == 270){
                            var tmp = wpx;
                            wpx = wpy;
                            wpy = -tmp;
                        } else if (num == 90){
                            var tmp = wpx;
                            wpx = -wpy;
                            wpy = tmp;
                        }
                        break;
                }
            }
            Result1 = Math.Abs(x1) + Math.Abs(y1);
            Result2 = Math.Abs(x2) + Math.Abs(y2);
        }
    }
}