using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day15 : Day{
        public override long Expected1 { get; set; } = 436;
        public override long Expected2 { get; set; } = 175594;

        private int _end = 2020;

        public override void Run(){
            var numbers = new Dictionary<long, (long, long?)>();
            for (int i = 0; i < Input[0].Split(',').Count() - 1; i++){
                var num = Input[0].Split(',')[i];
                numbers.Add(int.Parse(num), (i + 1, null));
            }
            var lastNum = long.Parse(Input[0].Split(',').Last());
            for(int i = numbers.Count() + 1; i < 30000000; i++){
                if(numbers.ContainsKey(lastNum)){
                    numbers[lastNum] = (i, numbers[lastNum].Item1);
                    lastNum = numbers[lastNum].Item1 - (numbers[lastNum].Item2 ?? 0);
                } else{
                    numbers[lastNum] = (i, null);
                    lastNum = 0;
                }
                if (i == 2019){
                    Result1 = lastNum;
                }
            }
            Result2 = lastNum;
        }
    }
}