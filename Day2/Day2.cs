using System;
using System.Linq;

namespace AdventOfCode2020
{
    public class Day2 : Day{
        public override int Expected1 { get; set; } = 514579;
        public override int Expected2 { get; set; } = 241861950;
        public override void Run(){
            var nums = Input.Select(x => int.Parse(x)).ToList();
            for(int i = 0; i < nums.Count(); i++){
                for(int j = i + 1; j < nums.Count(); j++){
                    for(int k = j + 1; k < nums.Count(); k++){
                        if(nums[i] + nums[j] + nums[k] == 2020){
                            Result2 = (nums[i] * nums[j] * nums[k]);
                        }
                    }
                    if(nums[i] + nums[j]== 2020){
                        Result1 = (nums[i] * nums[j]);
                    }
                }
            }
        }
    }
}