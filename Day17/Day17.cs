using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day17 : Day{
        public override long Expected1 { get; set; } = 112;
        public override long Expected2 { get; set; } = 848;

        private class Node{
            public int x{get;set;}
            public int y{get;set;}
            public int z{get;set;}
            public bool markedForDeletion{get;set;}

            public bool CheckNeighbor(Node otherNode){
                if (Math.Abs(x - otherNode.x) <= 1){
                    if(Math.Abs(y - otherNode.y) <= 1){
                        if(Math.Abs(z - otherNode.z) <= 1){
                            return true;
                        }
                    }
                }
                return false;
            }
            public List<Node> CreateNeighbors(){
                var result = new List<Node>();
                for(var x1 = x - 1; x1 <= x + 1; x1++){
                    for(var y1 = y - 1; y1 <= y + 1; y1++){
                        for(var z1 = z - 1; z1 <= z + 1; z1++){
                            if(x != x1 || y != y1 || z != z1) result.Add(new Node{x = x1, y = y1, z = z1, markedForDeletion = false});
                        }
                    }
                }
                return result;
            }
        }
        
        private class Node4{
            public int x{get;set;}
            public int y{get;set;}
            public int z{get;set;}
            public int w{get;set;}
            public bool markedForDeletion{get;set;}

            public bool CheckNeighbor(Node4 otherNode){
                if (Math.Abs(x - otherNode.x) <= 1){
                    if(Math.Abs(y - otherNode.y) <= 1){
                        if(Math.Abs(z - otherNode.z) <= 1){
                            if(Math.Abs(w - otherNode.w) <= 1){
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            public List<Node4> CreateNeighbors(){
                var result = new List<Node4>();
                for(var x1 = x - 1; x1 <= x + 1; x1++){
                    for(var y1 = y - 1; y1 <= y + 1; y1++){
                        for(var z1 = z - 1; z1 <= z + 1; z1++){
                            for(var w1 = w - 1; w1 <= w + 1; w1++){
                                if(x != x1 || y != y1 || z != z1 || w != w1) result.Add(new Node4{x = x1, y = y1, z = z1, w = w1, markedForDeletion = false});
                            }
                        }
                    }
                }
                return result;
            }
        }

        void Part1(){
            var counter = 0;
            var activeNodes = new List<Node>();
            foreach(var line in Input){
                var col = 0;
                foreach(var c in line){
                    if(c == '#'){
                        activeNodes.Add(new Node{
                            x = col, y = counter, z = 0, markedForDeletion = false
                        });
                    }
                    col++;
                }
                counter++;
            }
            for(var i = 0; i < 6; i++){
                var newNodes = new List<Node>();
                foreach(var node in activeNodes){
                    var neighborAmount = 0;
                    foreach(var otherNode in activeNodes){
                        if(node == otherNode) continue;
                        if(node.CheckNeighbor(otherNode)) neighborAmount++;
                    }
                    if(neighborAmount != 2 && neighborAmount != 3) node.markedForDeletion = true;
                    var neighborNodes = node.CreateNeighbors().Where(x => 
                        !activeNodes.Any(y => x.x == y.x && x.y == y.y && x.z == y.z)
                        && !newNodes.Any(y => x.x == y.x && x.y == y.y && x.z == y.z)
                    );
                    foreach(var neighbor in neighborNodes){
                        neighborAmount = 0;
                        foreach(var otherNode in activeNodes){
                            if(neighbor.CheckNeighbor(otherNode)) neighborAmount++;
                        }
                        if(neighborAmount == 3){
                            newNodes.Add(neighbor);
                        }
                    }
                }
                activeNodes = activeNodes.Where(x => !x.markedForDeletion).ToList();
                newNodes.ForEach(x => activeNodes.Add(x));
            }
            Result1 = activeNodes.Count;
        }

        void Part2(){
            var counter = 0;
            var activeNodes = new List<Node4>();
            foreach(var line in Input){
                var col = 0;
                foreach(var c in line){
                    if(c == '#'){
                        activeNodes.Add(new Node4{
                            x = col, y = counter, z = 0, w = 0, markedForDeletion = false
                        });
                    }
                    col++;
                }
                counter++;
            }
            for(var i = 0; i < 6; i++){
                var newNodes = new List<Node4>();
                foreach(var node in activeNodes){
                    var neighborAmount = 0;
                    foreach(var otherNode in activeNodes){
                        if(node == otherNode) continue;
                        if(node.CheckNeighbor(otherNode)) neighborAmount++;
                    }
                    if(neighborAmount != 2 && neighborAmount != 3) node.markedForDeletion = true;
                    var neighborNodes = node.CreateNeighbors().Where(x => 
                        !activeNodes.Any(y => x.x == y.x && x.y == y.y && x.z == y.z && x.w == y.w)
                        && !newNodes.Any(y => x.x == y.x && x.y == y.y && x.z == y.z && x.w == y.w)
                    );
                    foreach(var neighbor in neighborNodes){
                        neighborAmount = 0;
                        foreach(var otherNode in activeNodes){
                            if(neighbor.CheckNeighbor(otherNode)) neighborAmount++;
                        }
                        if(neighborAmount == 3){
                            newNodes.Add(neighbor);
                        }
                    }
                }
                activeNodes = activeNodes.Where(x => !x.markedForDeletion).ToList();
                newNodes.ForEach(x => activeNodes.Add(x));
            }
            Result2 = activeNodes.Count;
        }

        public override void Run(){
            Part1();
            Part2();
        }
    }
}