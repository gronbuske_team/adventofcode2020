using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2020
{
    public class Day3 : Day{
        public override int Expected1 { get; set; } = 7;
        public override int Expected2 { get; set; } = 336;

        private int GetTrees(int step, bool skip = false){
            var pos = 0;
            var trees = 0;
            Input.Where((_, i) => !skip || i % 2 == 0).ToList().Select(s => s.ToCharArray().Select(c => c == '#').ToList()).ToList().ForEach(p => {trees += p[pos % p.Count()] ? 1 : 0; pos += step; });
            return trees;
        }

        public override void Run(){
            Result1 = GetTrees(3);
            var poses = new int[]{1, 5, 7};
            Result2 = GetTrees(1, true) * Result1;
            foreach (int i in poses)
            {
                Result2 *= GetTrees(i);
            }
        }
    }
}