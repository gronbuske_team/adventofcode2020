using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day16 : Day{
        public override long Expected1 { get; set; } = 0;
        public override long Expected2 { get; set; } = 1;

        private class TicketField{
            public int firstStart{get;set;}
            public int firstEnd{get;set;}
            public int SecondStart{get;set;}
            public int SecondEnd{get;set;}
            public List<int> NotPossibleFields{get;set;}
        }

        public override void Run(){
            var counter = 0;
            var dict = new Dictionary<string, TicketField>();
            foreach(var line in Input){
                if(string.IsNullOrWhiteSpace(line)){
                    break;
                }
                var type = line.Split(": ")[0];
                var interval = line.Split(": ")[1];
                var intervals = interval.Split(" or ");
                var ticketField = new TicketField{
                    firstStart = int.Parse(intervals[0].Split("-")[0]),
                    firstEnd = int.Parse(intervals[0].Split("-")[1]),
                    SecondStart = int.Parse(intervals[1].Split("-")[0]),
                    SecondEnd = int.Parse(intervals[1].Split("-")[1]),
                    NotPossibleFields = new List<int>()
                };
                dict.Add(type, ticketField);
                counter++;
            }

            counter += 2;
            var ownTicket = Input[counter].Split(',').Select(x => int.Parse(x)).ToList();
            counter += 3;

            for (var i = counter; i < Input.Count(); i++){
                if(string.IsNullOrWhiteSpace(Input[i])){
                    break;
                }
                var ticket = Input[i].Split(',').Select(x => int.Parse(x));
                var allOk = true;
                foreach(var t in ticket){
                    var ok = false;
                    foreach(var field in dict){
                        if((t >= field.Value.firstStart && t <= field.Value.firstEnd) || (t >= field.Value.SecondStart && t <= field.Value.SecondEnd)){
                            ok = true;
                        }
                    }
                    if(!ok){
                        allOk = false;
                        Result1 += t;
                    }
                }
                if(allOk){
                    counter = 0;
                    foreach(var t in ticket){
                        foreach(var field in dict){
                            if(!((t >= field.Value.firstStart && t <= field.Value.firstEnd) || (t >= field.Value.SecondStart && t <= field.Value.SecondEnd))){
                                if(!field.Value.NotPossibleFields.Any(x => x == counter)) {
                                    field.Value.NotPossibleFields.Add(counter);
                                }
                            }
                        }
                        counter++;
                    }
                }
            }
            var done = false;
            while(!done){
                done = true;
                foreach(var d in dict){
                    if(d.Value.NotPossibleFields.Count() == 19){
                        for(int i = 0; i < 20; i++){
                            if(!d.Value.NotPossibleFields.Any(x => x == i)){
                                foreach(var t in dict){
                                    if(d.Key != t.Key && !t.Value.NotPossibleFields.Contains(i)){
                                        t.Value.NotPossibleFields.Add(i);
                                        done = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Result2 = 1;
            var x = dict.Select(x => x.Value.NotPossibleFields.FirstOrDefault());
            foreach(var d in dict){
                // Console.WriteLine(d.Key);
                // Console.WriteLine(d.Value.NotPossibleFields.FirstOrDefault());
                if (d.Key.StartsWith("departure")){
                    for(int i = 0; i < 20; i++){
                        if(!d.Value.NotPossibleFields.Any(x => x == i)){
                            Result2 *= ownTicket[i];
                            Console.WriteLine(d.Key);
                            Console.WriteLine(d.Value.NotPossibleFields.FirstOrDefault());
                            Console.WriteLine(ownTicket[i]);
                        }
                    }
                }
            }
        }
    }
}