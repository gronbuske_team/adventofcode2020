using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day4 : Day{
        public override int Expected1 { get; set; } = 10;
        public override int Expected2 { get; set; } = 6;

        private class Passport{
            public string BirthYear{get;set;}
            public string IssueYear{get;set;}
            public string ExpirationYear{get;set;}
            public string Height{get;set;}
            public string HairColour{get;set;}
            public string EyeColour{get;set;}
            public string PassportID{get;set;}
            public string CountryID{get;set;}
            
            public bool IsValid(){
                return BirthYear != null && IssueYear != null && ExpirationYear != null && Height != null && HairColour != null && EyeColour != null && PassportID != null;
            }

            public bool IsReallyValid(){
                return 
                    Regex.Match(BirthYear, "^19[2-9][0-9]|200[0-2]$").Success && 
                    Regex.Match(IssueYear, "^201[0-9]|2020$").Success && 
                    Regex.Match(ExpirationYear, "^202[0-9]|2030$").Success && 
                    (
                        Regex.Match(Height, "^1[5-8][0-9]cm$").Success || 
                        Regex.Match(Height, "^19[0-3]cm$").Success || 
                        Regex.Match(Height, "^59in$").Success || 
                        Regex.Match(Height, "^6[0-9]in$").Success || 
                        Regex.Match(Height, "^7[0-6]in$").Success
                    ) &&
                    Regex.Match(HairColour, "^[#][0-9a-f]{6}$").Success &&
                    new string[]{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}.Where(x => x == EyeColour).Count() == 1 && 
                    Regex.Match(PassportID, "^[0-9]{9}$").Success;
            }
        }

        public override void Run(){
            var currPassport = new Passport();
            foreach(var line in Input){
                if (string.IsNullOrWhiteSpace(line))
                {
                    Result1 += currPassport.IsValid() ? 1 : 0;
                    Result2 += currPassport.IsValid() && currPassport.IsReallyValid() ? 1 : 0;
                    currPassport = new Passport();
                }
                line.Split(" ").ToList().ForEach(x => {
                    switch(x.Split(":")[0]){
                        case "byr":
                            currPassport.BirthYear = x.Split(":")[1].Trim();
                            break;
                        case "iyr":
                            currPassport.IssueYear = x.Split(":")[1].Trim();
                            break;
                        case "eyr":
                            currPassport.ExpirationYear = x.Split(":")[1].Trim();
                            break;
                        case "hgt":
                            currPassport.Height = x.Split(":")[1].Trim();
                            break;
                        case "hcl":
                            currPassport.HairColour = x.Split(":")[1].Trim();
                            break;
                        case "ecl":
                            currPassport.EyeColour = x.Split(":")[1].Trim();
                            break;
                        case "pid":
                            currPassport.PassportID = x.Split(":")[1].Trim();
                            break;
                        case "cid":
                            currPassport.CountryID = x.Split(":")[1].Trim();
                            break;
                        default:
                            break;
                    }
                });
            }
            Result1 += currPassport.IsValid() ? 1 : 0;
            Result2 += currPassport.IsValid() && currPassport.IsReallyValid() ? 1 : 0;
        }
    }
}