using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    public class Day9 : Day{
        public override long Expected1 { get; set; } = 127;
        public override long Expected2 { get; set; } = 62;

        public override void Run(){
            var preamble = Input.Count() == 20 ? 5 : 25;
            var pastNumbers = new int[preamble];
            for (var i = 0; i < preamble; i++){
                pastNumbers[i] = int.Parse(Input[i]);
            }
            var counter = preamble;
            for (var i = preamble; i < Input.Count(); i++){
                var valid = false;
                var number = int.Parse(Input[i]);
                for (var j = 0; j < preamble; j++){
                    for (var k = j + 1; k < preamble; k++){
                        if(pastNumbers[j] + pastNumbers[k] == number) valid = true;
                    }
                }
                if(!valid && Result1 == 0) {
                    Result1 = number;
                    break;
                }
                pastNumbers[i%preamble] = int.Parse(Input[i]);
            }
            var queue = new List<int>();
            for (var i = 0; i < Input.Count(); i++){
                while (queue.Sum() > Result1){
                    queue.RemoveAt(0);
                }
                if (queue.Count() > 1 && queue.Sum() == Result1){
                    Result2 = queue.Min() + queue.Max();
                    break;
                }
                var number = int.Parse(Input[i]);
                queue.Add(number);
            }
        }
    }
}