using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    
    public class Bag{
        public string Name {get;set;}
        public List<Bag> Parents {get;set;}
        public Dictionary<Bag, int> Children {get;set;}
        public bool Checked {get;set;}
        public Bag(string name){
            Name = name;
            Parents = new List<Bag>();
            Children = new Dictionary<Bag, int>();
            Checked = false;
        }

        public int CheckParents(int counter){
            Checked = true;
            if(Parents.Any()){
                foreach(var p in Parents){
                    if(!p.Checked)
                        counter += p.CheckParents(1);
                }
            }
            return counter;
        }

        public int CheckAmount(int amount){
            if(Children.Any()){
                foreach(var c in Children){
                    amount += c.Value * c.Key.CheckAmount(1);
                }
            }
            return amount;
        }
    }
    public class Day7 : Day{
        public override long Expected1 { get; set; } = 4;
        public override long Expected2 { get; set; } = 32;

        public override void Run(){
            var dict = new Dictionary<string, Bag>();
            foreach(var line in Input){
                var split = line.Trim().TrimEnd('.').Split(" contain ");
                var name = split[0].TrimEnd('s');
                dict.TryAdd(name, new Bag(name));
                if(split[1] == "no other bags")
                    continue;
                split = split[1].Split(", ");
                foreach(var bag in split){
                    var nr = int.Parse(bag.Split(" ")[0]);
                    var childName = bag.Substring(bag.IndexOf(' ') + 1).TrimEnd('s');
                    dict.TryAdd(childName, new Bag(childName));
                    var child = dict[childName];
                    dict[name].Children.TryAdd(child, nr);
                    dict[childName].Parents.Add(dict[name]);
                }
            }
            var currentBag = "shiny gold bag";
            Result1 = dict[currentBag].CheckParents(0);
            Result2 = dict[currentBag].CheckAmount(0);
        }
    }
}